package com.mycompany.mywepapp;

import com.mycompany.mywepapp.user.User;
import com.mycompany.mywepapp.user.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.annotation.Rollback;

import java.util.Optional;

@DataJdbcTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
public class UserReposetoryTests {

    @Autowired private UserRepository repo;

    @Test
    public void testAddNew(){

        User user = new User();
        user.setEmail("test@gmail.com");
        user.setPassword("test");
        user.setFirstName("Mark");
        user.setLastName("Mustermann");

       User saveUser = repo.save(user);

        Assertions.assertThat(saveUser).isNotNull();
        Assertions.assertThat(saveUser.getId()).isGreaterThan(0);

    }

    @Test

    public void testListAll(){
        Iterable<User> users =repo.findAll();
        Assertions.assertThat(users).hasSizeGreaterThan(0);

        for (User user : users){

            System.out.println(user);
        }
    }

    @Test

    public void testUpdate(){
        Integer userId = 1 ;
        Optional<User> optionalUser =repo.findById(userId);

        User user = optionalUser.get();
        user.setPassword("testtt");
        repo.save(user);

        User updateUser= repo.findById(userId).get();
        Assertions.assertThat(updateUser.getPassword()).isEqualTo("testtt");

    }

    public void testGet(){

        Integer userId = 2 ;
        Optional<User> optionalUser =repo.findById(userId);

        Assertions.assertThat(optionalUser).isPresent();
        System.out.println(optionalUser.get());

    }

    @Test

    public void testDelet(){

        Integer userId = 2;
        repo.deleteById(userId);

        Optional<User> optionalUser =repo.findById(userId);
        Assertions.assertThat(optionalUser).isNotPresent();

    }

}
